import Vue from 'vue'
import VueRouter from 'vue-router'
import Layout from "@/layout/index.vue";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'share',
    component: () => import('../Share.vue')
  },
  {
    path: '/home',
    name: 'SysLayout',
    component: Layout,
    children: [
      {
        path: '/home',
        name: 'home',
        component: () => import('../views/Home.vue')
      },
      {
        path: '/setting',
        name: 'setting',
        component: () => import('../views/setting/index.vue'),
      },
      {
        path: '/kmusic',
        name: 'kmusic',
        component: () => import('../views/KuwoMusic.vue')
      },
      {
        path: '/map',
        name: 'map',
        component: () => import('../views/Map.vue')
      },
      {
        path: '/staticWeather',
        name: 'staticWeather',
        component: () => import('../views/StaticWeather.vue')
      },
      {
        path: '/assistant',
        name: 'assistant',
        component: () => import('../views/VoiceAssistant.vue')
      },
      {
        path: '/xima',
        name: 'xima',
        component: () => import('../views/XiMa.vue')
      },
      {
        path: '/airconditioner',
        name: 'airconditioner',
        component: () => import('../views/AirConditioner.vue')
      },
      {
        path: '/backstage',
        name: 'backstage',
        component: () => import('../views/Stage.vue')
      },
      {
        path: '/screen',
        name: 'screen',
        component: () => import('../views/SplitScreen.vue')
      },
      {
        path : '/user_manual',
        name : 'user_Manual',
        component: () => import('../views/setting/userManual.vue'),
      },
      {
        path: '/camera',
        name: 'camera',
        component: () => import('../views/page/camera.vue')
      },
      {
        path: '/photo',
        name: 'photo',
        component: () => import('../views/page/photo.vue')
      },
      {
        path: '/userManual',
        name: 'userManual',
        component: () => import('../views/page/userManual.vue')
      },
      {
        path: '/download',
        name: 'download',
        component: () => import('../views/page/downLoad.vue')
      },
      {
        path: '/fileManage',
        name: 'fileManage',
        component: () => import('../views/page/fileManage.vue')
      },
      {
        path: '/fault',
        name: 'fault',
        component: () => import('../views/page/fault.vue')
      },
      {
        path: '/phone',
        name: 'phone',
        component: () => import('../views/pageThree/Phone.vue'),
      },
      {
        path: '/diant/:index',
        name: 'diant',
        component: () => import('../views/pageThree/DianTai.vue'),
      },
      {
        path: '/yinx',
        name: 'yinx',
        component: () => import('../views/pageThree/YinXiang.vue'),
      },
      {
        path: '/cari',
        name: 'cari',
        component: () => import('../views/pageThree/CarImg.vue'),
      },
      {
        path: '/kugou',
        name: 'kugou',
        component: () => import('../views/pageThree/KuGou.vue'),
      },
      {
        path: '/cgtop',
        name: 'cgtop',
        component: () => import('../views/pageThree/CgTop.vue'),
      },
      {
        path: '/shic',
        name: 'shic',
        component: () => import('../views/pageThree/ByShic.vue'),
      },
      {
        path: '/xingx',
        name: 'xingx',
        component: () => import('../views/pageThree/XingChe.vue'),
      },
      {
        path: '/beforeCharge',
        name: 'beforeCharge',
        component: () => import('../components/setting/BeforeCharge.vue')
      },
      {
        path: '/energyList',
        name: 'energyList',
        component: () => import('../components/setting/EnergyList.vue')
      },
      {
        path: '/parkCar',
        name: 'parkCar',
        component: () => import('../components/setting/ParkCar.vue')
      },
      {
        path: '/energyFeed',
        name: 'energyFeed',
        component: () => import('../components/setting/EnergyFeed.vue')
      },
      {
        path: '/agreement',
        name: 'agreement',
        component: () => import('../views/agreement.vue')
      }
    ]
  }
]

const router = new VueRouter({
  routes,
  scrollBehavior (to, from, savedPosition) {
    return savedPosition || { x: 0, y: 0 }
  }
})


const originaPush = VueRouter.prototype.push;

VueRouter.prototype.push = function push(location) {
  return originaPush.call(this, location).catch(err => err)
}

export default router
