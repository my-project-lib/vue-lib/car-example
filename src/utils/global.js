document.addEventListener('DOMContentLoaded', (event) => {
  let lastTouchTime = 0
  let touchEndHandler = function (event) {
    let currentTime = new Date().getTime()
    let timeDifference = currentTime - lastTouchTime
    if (timeDifference < 500) {
      event.preventDefault()
    }
    lastTouchTime = currentTime
  }
  document.addEventListener('touchend', touchEndHandler, false)
})
