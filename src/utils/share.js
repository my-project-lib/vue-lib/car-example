export function getAppInfo() {
    const ua = navigator.userAgent;
    const co = document.cookie;
    const ur = /auto_(iphone|android)(;|%3B|\/)(\d+\.\d+\.\d+)/i;
    const cr = /.*app_key=auto_(iphone|android)(.*)appVer=(\d+\.\d+\.\d+)/i;
    const match = ua.match(ur) || co.match(cr);

    return {
        isApp: /autohomeapp/.test(ua) || ur.test(ua) || cr.test(co),
        appKey: match && match[1],
        appVer: match && match[3],
    };
}