const getters = {
    bottom:state => state.bottom.bottomIcon,
    showMask:state => state.header.showMask,
    topLine:state => state.header.topLine
  }
  export default getters