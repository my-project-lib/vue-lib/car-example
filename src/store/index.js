import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import bottom from './modules/bottom'
import header from './modules/header'
import pageFour from './modules/pageFour'
import home from "@/store/modules/home";
Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    bottom,
    header,
    pageFour,
    home
  },
  getters
})

export default store