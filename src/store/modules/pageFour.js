const state = {
  showDialog: false
}

const mutations = {
  changeShowDialog(state, data) {
    state.showDialog = data
  }
}

export default {
  namespaced: true,
  state,
  mutations
}