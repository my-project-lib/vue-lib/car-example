const state = {
    musicDialogShow: false,
}

const mutations = {
    changeMusicDialogShow(state, data) {
        state.musicDialogShow = data
    },
}

export default {
    namespaced: true,
    state,
    mutations
}