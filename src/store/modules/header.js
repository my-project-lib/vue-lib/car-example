const state = {
    showMask: "",
    headerDark:false,
    headerHidden:false
  }
  
  const mutations =  {
       changeMask(state, data) {
        state.showMask = data
      },
      changeHeaderDark(state, data) {
        state.headerDark = data
      },
      changeHeaderHidden(state, data) {
        state.headerHidden = data
      },
  }
  
  export default {
    namespaced: true,
    state,
    mutations
  }