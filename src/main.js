import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueTouch from 'vue-touch'

import { px2vw }from "./utils/px2vw"
import './utils/global'
import './styles/rest.css'
import './styles/global.scss'

import UCard from "@/components/Card.vue";
import USwitch from "@/components/Switch.vue";
import UTabs from "@/components/Tabs.vue";

Vue.use(VueTouch, { name: 'v-touch' })

Vue.component("u-card", UCard)
Vue.component("u-switch", USwitch)
Vue.component("u-tabs", UTabs)

Vue.config.productionTip = false
Vue.prototype.$px2vw = px2vw
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
