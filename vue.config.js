const { defineConfig } = require("@vue/cli-service");
const path = require("path");
const CompressionPlugin = require("compression-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");

const name ='qiche' // page title
const resolve = dir => path.join(__dirname, dir);
let productionGzip = /\.(js|css|json|txt|html|xml|svg|ttf|woff|woff2)$/i;

module.exports = defineConfig({
  transpileDependencies: true,
  publicPath: './',
  outputDir: 'haibao',
  productionSourceMap: false,
  configureWebpack: {
    name: name,
    resolve: {
      alias: {
        '@': resolve('src')
      }
    },
    optimization: {
      minimize: true,
      minimizer: [
        new CompressionPlugin({
          filename: '[path][base].br',
          algorithm: 'brotliCompress',
          test: productionGzip,
          threshold: 8192,
          minRatio: 0.8,
        }),
        new TerserPlugin({
          terserOptions: {
            compress: {
              drop_console: true // 去除console.log
            }
          }
        })
      ]
    }
  },
  lintOnSave: false,// 取消eslint设置
  chainWebpack: config => {
    config.module.rule("svg").exclude.add(resolve("src/icons"));
    config.module
      .rule("icons")
      .test(/\.svg$/)
      .include.add(resolve("src/icons")) // 上下文变化
      .end() // 返回上下文
      .use("svg-sprite-loader")
      .loader("svg-sprite-loader")
      .options({ symbolId: "icon-[name]" });
      config.optimization.minimizer('terser').tap((args) => {
        args[0].terserOptions.compress.drop_console = true
        return args
      })
  }
});
